//
// Created by max on 16.09.23.
//

#include <api/sysinfo.pb.h>
#include <api/sysinfo.grpc.pb.h>

#include <grpc++/grpc++.h>

#include <iostream>
#include <string>

int main(int argc, const char* argv[])
{
    std::string serviceIp = argc > 1 ? argv[1] : "127.0.0.1:42424";

    auto channel = grpc::CreateChannel(serviceIp, grpc::InsecureChannelCredentials());

    auto cli = sysinfo::api::SysInfo::NewStub(channel);

    google::protobuf::Empty empty;
    sysinfo::api::Information info;
    grpc::ClientContext context;

    auto status = cli->GetInformation(&context, empty, &info);

    if (status.ok())
    {
        std::cout << "Node info:\n"
                  << "\t" << info.devicename() << '\n'
                  << "\t" << info.osversion() << '\n'
                  << "\t" << info.serialnumber() << '\n'
                  << "\t" << info.description() << '\n'
                  << std::endl;
    }
    else
    {
        std::cerr << status.error_message();
    }

    return 0;
}