##@brief generate
##@param in,required PROTO_FILES
##@param in,optional PROTO_INCLUDES
##@param in,required OUT_DIR
##@param out,required OUT_SRCS
##@param out,required OUT_HDRS
function(grpc_generate_cpp)
    set(args OUT_SRCS OUT_HDRS OUT_DIR)
    set(inp_args PROTO_FILES PROTO_INCLUDES)

    cmake_parse_arguments(VAR "" "${args}" "${inp_args}" ${ARGN})

    if(NOT (DEFINED VAR_OUT_SRCS AND DEFINED VAR_OUT_HDRS))
        message(SEND_ERROR "No output vars")
        return()
    endif()

    if(NOT VAR_PROTO_FILES)
        message(SEND_ERROR "no input files")
    endif()

    set(SRCS)
    set(HDRS)

    set(OUT_DIR ${CMAKE_CURRENT_BINARY_DIR})
    if(DEFINED VAR_OUT_DIR)
        get_filename_component(OUT_DIR ${VAR_OUT_DIR} ABSOLUTE)
    endif()

    set(_protobuf_include_path
            -I ${CMAKE_CURRENT_SOURCE_DIR}
    )
    if(DEFINED VAR_PROTO_INCLUDES)
        list(APPEND _protobuf_include_path ${VAR_PROTO_INCLUDES})
    endif()

    if (NOT DEFINED Protobuf_PROTOC_EXECUTABLE)
        find_program(PROTOC NAMES protoc)
    else ()
        set(PROTOC ${Protobuf_PROTOC_EXECUTABLE})
    endif ()

    if (NOT DEFINED PROTOC OR PROTOC STREQUAL "")
        message(FATAL_ERROR "no protoc")
    endif ()

    if (TARGET gRPC)
        message("Try get gRPC::grpc_cpp_plugin")
        get_target_property(GRPC_CPP_PLUGIN_PROGRAM gRPC::grpc_cpp_plugin
                IMPORTED_LOCATION_RELEASE)
    else ()
        message("Try find grpc_cpp_plugin")
        find_program(GRPC_CPP_PLUGIN_PROGRAM
            NAMES
                grpc_cpp_plugin
        )
        if (GRPC_CPP_PLUGIN_PROGRAM STREQUAL "")
            message(FATAL_ERROR "Try find grpc_cpp_plugin failed")
        endif ()
        #get_filename_component(GRPC_CPP_PLUGIN_PROGRAM  ${GRPC_CPP_PLUGIN_PROGRAM} NAME)
    endif ()


    foreach(FIL ${VAR_PROTO_FILES})
        get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
        get_filename_component(FIL_WE ${FIL} NAME_WE)
        get_filename_component(FIL_DIR ${FIL} DIRECTORY)

        list(APPEND SRCS "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.pb.cc")
        list(APPEND SRCS "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.grpc.pb.cc")

        list(APPEND HDRS "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.grpc.pb.h")
        list(APPEND HDRS "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.pb.h")

        add_custom_command(
                OUTPUT "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.pb.cc"
                "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.pb.h"
                "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.grpc.pb.cc"
                "${OUT_DIR}/${FIL_DIR}/${FIL_WE}.grpc.pb.h"
                COMMAND  ${PROTOC}
                ARGS --grpc_out=${OUT_DIR}
                --cpp_out=${OUT_DIR}
                --plugin=protoc-gen-grpc=${GRPC_CPP_PLUGIN_PROGRAM}
                ${_protobuf_include_path} ${ABS_FIL}
                DEPENDS ${ABS_FIL}
                COMMENT "Running gRPC C++ protocol buffer compiler on ${FIL}"
                VERBATIM)
    endforeach()

    set_source_files_properties(${SRCS} ${HDRS} PROPERTIES GENERATED TRUE)
    # "return" names of generated files
    set(${VAR_OUT_SRCS} ${SRCS} PARENT_SCOPE)
    set(${VAR_OUT_HDRS} ${HDRS} PARENT_SCOPE)
endfunction()