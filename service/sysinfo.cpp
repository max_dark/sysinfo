//
// Created by max on 15.09.23.
//

#include "sysinfo.h"

#include <sys/utsname.h>


grpc::Status sysinfo::SysInfo::GetInformation(::grpc::ServerContext *context,
                                              const ::google::protobuf::Empty *request,
                                              ::sysinfo::api::Information *response)
{
    utsname info{};

    auto ret = uname(&info);

    if (ret)
    {
        auto code = errno;

        return grpc::Status{grpc::StatusCode::INTERNAL, "Unable get uname: " + std::to_string(code)};
    }

    response->set_devicename(info.nodename);
    response->set_description(info.version);
    response->set_osversion(info.release);
    response->set_serialnumber(getSerialCode());

    return grpc::Status::OK;
}

void sysinfo::SysInfo::setSerialCode(std::string serial_code)
{
    serialCode = std::move(serial_code);
}

const std::string& sysinfo::SysInfo::getSerialCode() const
{
    return serialCode;
}
