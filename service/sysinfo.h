//
// Created by max on 15.09.23.
//

#ifndef SYSINFOD_SYSINFO_H
#define SYSINFOD_SYSINFO_H

#include <api/sysinfo.pb.h>
#include <api/sysinfo.grpc.pb.h>

#include <string>

namespace sysinfo
{
class SysInfo: public api::SysInfo::Service
{
public:
    grpc::Status GetInformation(::grpc::ServerContext* context, const ::google::protobuf::Empty* request, ::sysinfo::api::Information* response) override;

    void setSerialCode(std::string serial_code);
    const std::string& getSerialCode() const;
private:
    std::string serialCode = "not set";
};

} // namespace sysinfo

#endif //SYSINFOD_SYSINFO_H
