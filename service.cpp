#include "service/sysinfo.h"
#include <grpc++/grpc++.h>

#include <sys/utsname.h>

int main(int argc, const char ** argv)
{
    //TODO: parse argv
    //TODO: use `fork` may be?

    utsname info{};

    auto ret = uname(&info);

    if (ret)
    {
        auto code = errno;

        std::cerr << "Unable get uname: " << code << std::endl;
        return code;
    }

    std::cout << "Uname:" << '\n'
              << '\t' << "nodename " << info.nodename << '\n'
              << '\t' << "sysname " << info.sysname << '\n'
              << '\t' << "machine " << info.machine << '\n'
              << '\t' << "release " << info.release << '\n'
              << '\t' << "version " << info.version << '\n'
              << '\t' << "domainname " << info.domainname << '\n'
              << std::endl;
    sysinfo::SysInfo service;

    service.setSerialCode("TODO: set serial code");

    auto server = grpc::ServerBuilder()
            .AddListeningPort("0.0.0.0:42424", grpc::InsecureServerCredentials())
            .RegisterService(&service)
            .BuildAndStart();


    server->Wait();
    return 0;
}
